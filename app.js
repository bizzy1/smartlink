const express = require('express');
const path = require('path');
const geoip = require('geoip-lite');
const app = express();
const port = 8090;
const PageSwitcher = require('./page_switcher');

const server = app.listen(port, (err) => {
    if (err) {
        return console.log('something bad happened', err)
    }
    console.log(`server is listening on ${port}`)
})

app.use(express.static('public'));

app.get('/', (request, response) => {
    let ip = request.connection.remoteAddress.match(/\d{1,}.\d{1,}.\d{1,}.\d{1,}$/)[0];
    // mock ip
    // ip = '185.41.250.238';

    const pathToFile = PageSwitcher.getPage(ip);
    
    response.sendFile(path.join(__dirname, '/public' + pathToFile));
});