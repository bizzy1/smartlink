const geoip = require('geoip-lite');
const COUNTRIES = require('./countries');


function getPage(ip) {
    const country = getCountry(ip);
    if (!country) {
        console.log('country defined in getIp module');
        return '/default/index.html';
    } 
    
    const offer = getOffer(country);

    return '/' + offer + '/index.html';
}

function getCountry(ip) {
    const country = geoip.lookup(ip).country;
    return country;
}

function getOffer(country) {
    let countryData = COUNTRIES.countries.find(item => item.country === country);
    if (!countryData) {
        console.log('country defined in COUNTRIES data');
        return 'default';
    } 
    return countryData.offer;
}

module.exports.getPage = getPage;