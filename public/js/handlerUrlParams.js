
$(function(){
    let searchParams = new URLSearchParams(window.location.search);
    let land = $('#linkToLand').text();
    $('body').append(
        `<script>
            const ase = document.querySelectorAll('a');
            for(let i = 0; i < ase.length; i++){
                ase[i].addEventListener('click', function(e){
                    e.preventDefault();
                    let url = '${land}?offerId=85'
                    if (${searchParams.has('pid')}) url += '&pid=${searchParams.get('pid')}'
                    window.location = url;
                })
            }
        </script>`
    );
});
