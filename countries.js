const countries = [
    {
        country: 'EE',
        offer: 29
    },
    {
        country: 'LV',
        offer: 29
    },
    {
        country: 'LT',
        offer: 29
    },
    {
        country: 'RU',
        offer: 26
    },
    {
        country: 'KZ',
        offer: 26
    },
    {
        country: 'PL',
        offer: 5
    },
    {
        country: 'IT',
        offer: 15
    },
    {
        country: 'DE',
        offer: 18
    },
    {
        country: 'AT',
        offer: 18
    },
    {
        country: 'CH',
        offer: 18
    },
    {
        country: 'ES',
        offer: 22
    },
    {
        country: 'GT',
        offer: 22
    },
    {
        country: 'SV',
        offer: 22
    },
    {
        country: 'PA',
        offer: 22
    },
    {
        country: 'EC',
        offer: 22
    },
    {
        country: 'CR',
        offer: 22
    },
    {
        country: 'AR',
        offer: 22
    },
    {
        country: 'PE',
        offer: 22
    },
    {
        country: 'MX',
        offer: 22
    },
    {
        country: 'CO',
        offer: 22
    },
    {
        country: 'CL',
        offer: 22
    },
    {
        country: 'PT',
        offer: 28
    },
    {
        country: 'TR',
        offer: 40
    },
    {
        country: 'GR',
        offer: 51
    },
    {
        country: 'FR',
        offer: 73
    },
    {
        country: 'UA',
        offer: 15
    }
]

module.exports.countries = countries;
